# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Post.create(title: 'Welcome!', body: 'This is the first post in this Ruby on Rails application. In the event you weren\'t aware, this is kind of a big deal')
Comment.create(post_id: 1, body: 'Sorry, but this isn\'t really an application. It\'s more of a joke, really.')
Comment.create(post_id: 1, body: 'Your presence here is unacceptable. Please remove yourself form this forum before we delete your existence')

Post.create(title: 'Thanks...', body: '... for patronizing this application. Due to some internal conflicts, we\'ve decided to discontinue this site. It was fun while it lasted, but alas -- all good things must come to an end')
Comment.create(post_id: 2, body: 'Eugh, thank GOD! This was the worst application on the planet, and the internet died a little thanks to its existence')